
---
date: 2019-11-05
title: "Ny hemsida till VoteIT"
description: "Efter många års utveckling får nu VoteIT en ny hemsida"
categories: ["Projekt"]
---

Hemsidan är byggd i [Hugo](https://gohugo.io) för att kunna erbjuda stöd för flerspråkighet.
