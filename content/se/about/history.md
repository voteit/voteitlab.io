---
title: History
linktitle: History
description: History about VoteIT
date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01
menu:
  docs:
    parent: "about"
    weight: 20
weight: 20
sections_weight: 20
draft: false
toc: true
---

Föreningen VoteITs syfte är ”att bidra till att mötesprogramvaran VoteIT vårdas, utvecklas och används samt att sprida kunskap kring nätbaserade beslutsmöten”. Föreningen bildades 2011 av 10 olika organisationer som ville skapa ett samarbetsorgan för att fortsätta utveckla VoteIT och nätdemokratin.

Programvaran VoteIT började som en plattform för att arrangera webbmöten och på så sätt göra det lättare att hålla möten med deltagare från hela landet genom att inte ha en fysisk sammankomst. Den funktionaliteten finns kvar, men VoteIT har även utvecklats till att vara en plattform för underlätta fysiska möten. Verksamheten bedrivs ideellt och är finansierad av medlemmarna.
Medlemskap

Alla privatpersoner och organisationer som delar föreningens intresse för nätdemokrati och är intresserade av att bidra till utvecklingen av VoteIT är välkomna att bli medlemmar i föreningen VoteIT. För att bli medlem skickar man ett mail till styrelsen och betalar in medlemsavgiften. Föreningens medlemsavgift är 10 000 kr per år.
Medlemsorganisationer

    AIK
    Betahaus
    Djurens rätt
    Junis
    LRF Ungdomen
    Lärarförbundet
    Mattecentrum
    Miljöpartiet de gröna
    Naturskyddsföreningen
    Nykterhetsrörelsens Scoutförbund
    RFSL
    RFSU
    Rädda Barnen
    Scouterna
    Socialdemokraterna
    Studiefrämjandet
    Svenska Kennelklubben
    Sveriges Blåbandsungdom
    Sveriges förenade studentkårer (SFS)
    Sverok
    Ungdomens nykterhetsförbund (UNF)

Styrelse

Styrelsen väljs av medlemmarna på föreningens årsmöte. Nuvarande ledamöter i styrelsen är:

    Anders Hultman
    Robin Harms Oredsson
    Linnea Risinger
    Hans Cruse

Stadgar

Stadgarna fastställdes vid det konstituerande årsmötet i december 2011 och finns att läsa här.
