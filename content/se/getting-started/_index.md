---
title: Kom igång
linktitle: Överblick
description: Quick start and guides for installing Hugo on your preferred operating system.
date: 2017-02-01
publishdate: 2017-02-01
lastmod: 2017-02-01
categories: [kom igång]
keywords: [usage,docs]
menu:
  docs:
    parent: "kom-igang"
    weight: 1
weight: 0001	#rem
draft: false
aliases: [/overview/introduction/]
toc: false
---

If this is your first time using Hugo and you've [already installed Hugo on your machine][installed], we recommend the [quick start][].

[installed]: /getting-started/installing/
[quick start]: /getting-started/quick-start/
