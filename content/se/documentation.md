---
title: Dokumentation
linktitle: Dokumentation
description: VoteIT Dokumentation beskriver hur VoteIT fungerar. VoteIT är Fri och Öppen mjukvara för digitala beslutsfattande möten. Det är skrivet i Python och utvecklat av föreningen VoteIT och föreningen Betahaus samt open source-utvecklare.
date: 2017-02-01
publishdate: 2017-02-01
menu:
  main:
    parent: "section name"
    weight: 01
weight: 01	#rem
draft: false
slug:
aliases: []
toc: false
layout: documentation-home
isCJKLanguage: true
---
VoteIT Dokumentation beskriver hur VoteIT fungerar. VoteIT är **Fri och Öppen mjukvara för digitala beslutsfattande möten.** Det är skrivet i Python och utvecklat av [VoteIT](https://github.com/voteit), [Betahaus](https://github.com/BetahausNGO) och [bidragsgivande vänner](https://github.com/voteit/voteit.core/graphs/contributors).

Här under kommer du hitta några av de mest vanliga och hjälpsamma sidor från vår dokumentation.
