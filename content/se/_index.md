---
title: "Fri och Öppen Mjukvara för beslutsfattande möten"
date: 2017-03-02T12:00:00-05:00
features:
  - heading: Talartid
    image_path: /images/icon-fast.svg
    tagline: Hantera talare och deras talartid.
    copy: VoteIT hjälper dig att enkelt hantera talare inklusive att enkelt låta dig hålla koll och föra protokoll på talartid.

  - heading: Hantera Agenda
    image_path: /images/icon-content-management.svg
    tagline: Flexibel agendahantering. En dröm som blir sann för användare, ordförande och presidium.

  - heading: Taggar
    image_path: /images/icon-shortcodes.svg
    tagline: VoteITs taggar kan vara ditt mötes gömda superkraft.
    copy: Du kanske börjar älska möjligheten att hålla diskussioner på ämnet och enkelt att följa. Taggar kan hjälpa dina mötesdeltagare att njuta av diskussioner som stödjer utveckling och beslut för framtiden.

  - heading: Inbyggda Omröstningsmetoder
    image_path: /images/icon-built-in-templates.svg
    tagline: VoteIT har alla vanliga omröstningsmetoder inbyggda för att få dina omröstningar gjorda snabbt och enkelt.
    copy: VoteIT har vanliga och ovanliga omröstningsmetoder för att utföra de mest passande omröstningsmetoder. Det finns omröstningsmetoder för varje situation med dina behov för beslutsfattande.

  - heading: Flerspråkigt och i18n
    image_path: /images/icon-multilingual2.svg
    tagline: Stödjer beslutsfattande för alla.
    copy: VoteIT har fullt i18n-stöd för flerspråkighet och funktionalitet med utvecklar- och användarupplevelse.

  - heading: Anpassningar
    image_path: /images/icon-custom-outputs.svg
    tagline: Är funktionalitet inte nog?
    copy: VoteIT tillåter anpassa funktionaliteten, utseendet och känslan av din mötesupplevelse. Logotyper, funktioner, typsnitt, färger - det är möjligt att ändra om det och mer. Du kan anpassa VoteIT för att dina användare ska känna igen sig och för att det ska passa med din organisations bildspråk, färgval och t.ex. varumärke. Om du inte kan göra anpassningarna själv så finns det hjälp att få från gemenskapen men också från professionella.
# sections:
#   - heading: "Hundratals Teman"
#     cta: Check out the Hugo's themes.
#     link: http://themes.gohugo.io/
#     color_classes: bg-accent-color white
#     image: /images/homepage-screenshot-hugo-themes.jpg
#     copy: "Hugo provides a robust theming system that is easy to implement but capable of producing even the most complicated websites."
#   - heading: "Kapabel Flexibilitet"
#     cta: Kom igång.
#     link: mallar/
#     color_classes: bg-primary-color-light black
#     image: /images/home-page-templating-example.png
#     copy: "Hugo's Go-based templating provides just the right amount of logic to build anything from the simple to complex. If you prefer Jade/Pug-like syntax, you can also use Amber, Ace, or any combination of the three."
---

VoteIT är Fri och Öppen Källkodmjukvara för beslutsfattande möten. Med dess fantastiska snabbhet och flexibilitet gör VoteIT formella, beslutsfattande möten roliga igen.
