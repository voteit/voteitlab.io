---

title: Junis
date: 2019-02-02
description: "Showcase: \"\""
siteURL: https://www.junis.se/
byline: ""
---

Junis är barnens organisation. Vi finns i hela landet och ger möjlighet till roliga och utvecklande fritidsaktiviteter där barnen själva får vara med och bestämma. Verksamheten sker i nykter miljö med engagerade och utbildade ledare.
