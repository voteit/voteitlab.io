**Matrix** is an communication tool for voice, video and text communication. Matrix is an open protocol with different user clients on all different platforms. One of these clients is [Riot.im](https://about.riot.im/)

* [VoteIT Matrix Community](https://matrix.to/#/+voteit:matrix.org)
* [VoteIT Matrix Room](https://matrix.to/#/!EUyweVXSAklivKNoMe:matrix.org?via=matrix.org)
