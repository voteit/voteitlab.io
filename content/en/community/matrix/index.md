---

title: Matrix Communication Platform
date: 2018-04-24
description: "Community: \"Many people gather to chat on the Matrix network\""
siteURL: https://matrix.to/#/+voteit:matrix.org
byline: "[VoteIT](https://voteit.se/)"

---
