---
title: VoteIT Documentation
linktitle: Documentation
description: VoteIT is Free and Open Source software for decisionmaking meetings. It is written in Python and developed by the NGO VoteIT and the NGO Betahaus as well as open source friends.
date: 2017-02-01
publishdate: 2017-02-01
menu:
  main:
    parent: "section name"
    weight: 01
weight: 01	#rem
draft: false
slug:
aliases: []
toc: false
layout: documentation-home
---
VoteIT is **Free and Open Source software for decisionmaking meetings.** It's written in Python and developed by [VoteIT](https://github.com/voteit), [Betahaus](https://github.com/betahausNGO) and [contributing friends](https://github.com/voteit/voteit.core/graphs/contributors).

Below you will find some of the most common and helpful pages from our documentation.
