---
title: "Free and Open Source software for decisionmaking meetings"
date: 2017-03-02T12:00:00-05:00
features:
  - heading: Speaker Management
    image_path: /images/icon-fast.svg
    tagline: What's smart about managing speakers on a paper list?
    copy: VoteIT features simple and clear management of speakers at your meeting and their speaking time. After a try, you do not want to go back to paper based speaker lists and unsynced timer clocks.

  - heading: Manage Agenda
    image_path: /images/icon-content-management.svg
    tagline: Flexible agenda management. A dream come true for users, chairmen and presidium.
    copy: VoteIT provides simple and flexible management of your meeting agenda. You can manage your meeting agenda more dynamically than ever before. Never refer to outdated paper printouts again.

  - heading: Hashtags
    image_path: /images/icon-shortcodes.svg
    tagline: VoteIT's hashtags can be your meeting's hidden superpower.
    copy: You might start loving the ability to keep discussions on topic and easy to follow. Hashtags can help your meeting participants to enjoy discussion that supports progress and decisions for the future.

  - heading: Built-in Voting Methods
    image_path: /images/icon-built-in-templates.svg
    tagline: VoteIT has as all common voting methods built in to get your vote done quickly.
    copy: VoteIT features usual and unusual voting methods to carry out suitable voting processes. There are voting methods for every situation with your decisionmaking needs.

  - heading: Multilingual and i18n
    image_path: /images/icon-multilingual2.svg
    tagline: Supporting decisionmaking for everyone.
    copy: VoteIT provides full i18n support for multi-language functionality with a development and usage experience.

  - heading: Customizations
    image_path: /images/icon-custom-outputs.svg
    tagline: Functionality not enough?
    copy: VoteIT enables you to customize the functionality, look and feel of your meeting experience. Logos, features, fonts, coloring -  it's possible to change around that and more. If you cannot do it yourself there is help to get from the community and also from professionals.
# sections:
#  - heading: "Hundreds of Themes"
#     cta: Check out the Hugo themes.
#     link: https://themes.gohugo.io/
#     color_classes: bg-accent-color white
#     image: /images/homepage-screenshot-hugo-themes.jpg
#     copy: "Hugo provides a robust theming system that # is easy to implement but capable of producing even the most complicated websites."
#   - heading: "Capable Flexibility"
#     cta: Get Started.
#     link: templates/
#     color_classes: bg-primary-color-light black
#     image: /images/home-page-templating-example.png
#     copy: "Hugo's Go-based templating provides just the right amount of logic to build anything from the simple to complex. If you prefer Jade/Pug-like syntax, you can also use Amber, Ace, or any combination of the three."
---

VoteIT is Free and Open Source software for decisionmaking meetings. With its fantastic speed and flexibility, VoteIT makes formal decisionmaking meetings fun again. Cheers to a fun way of running decisionmaking meetings.
