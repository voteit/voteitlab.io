[![Netlify Status](https://api.netlify.com/api/v1/badges/e0dbbfc7-34f1-4393-a679-c16e80162705/deploy-status)](https://app.netlify.com/sites/gohugoio/deploys)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](https://voteit.se/contribute/documentation/)

# VoteIT Website and Documentation

Project and Documentation site for [VoteIT](https://github.com/voteit/voteit), the free and open source software for decisionmaking meetings.

## Contributing

We welcome contributions to VoteIT of any kind including documentation, suggestions, bug reports, pull requests etc. Also check out our [contribution guide](https://voteit.se/contribute/documentation/). We would love to hear from you.

Note that this repository contains solely the documentation for VoteIT. For contributions that aren't documentation-related please refer to the [voteit](https://github.com/voteit/voteit) repository.

*Pull requests shall **only** contain changes to the actual documentation. However, changes on the code base of VoteIT **and** the documentation shall be a single, atomic pull request in the [voteit](https://github.com/voteit/voteit) repository.*

Spelling fixes are most welcomed, and if you want to contribute longer sections to the documentation, it would be great if you had these in mind when writing:

* Short is good. People go to the library to read novels. If there is more than one way to _do a thing_ in VoteIT, describe the current _best practice_ (avoid "… but you can also do …" and "… in older versions of VoteIT you had to …".
* For examples, try to find short snippets that teaches people about the concept. If the example is also useful as-is (copy and paste), then great, but don't list long and similar examples just so people can use them on their sites.
* VoteIT has users from all over the world, so an easy to understand and [simple English](https://simple.wikipedia.org/wiki/Basic_English) is good.

## Branches

* The `master` branch is where the site is automatically built from, and is the place to put changes relevant to the current VoteIT version.
* The `next` branch is where we store changes that is related to the next VoteIT release. This can be previewed here: https://next--gohugoio.netlify.com/

## Build

To view the documentation site locally, you need to clone this repository:

```bash
git clone https://github.com/voteit/voteit.github.io.git
```


Then to view the docs in your browser, run Hugo and open up the link:

```bash
▶ hugo server

Started building sites ...
.
.
Serving pages from memory
Web Server is available at http://localhost:1313/ (bind address 127.0.0.1)
Press Ctrl+C to stop
```
